from copy import deepcopy
from math import sqrt

from src.utils import print_green


class Sudoku:
    def __init__(self, dim, matrix=None):
        """
        Initializare pentru un joc Sudoku
        :param dim: dimensiunea
        :param matrix: reprezentare interna
        """
        self.dim = dim
        self.area_dim = int(sqrt(dim))
        if matrix:
            self.matrix = matrix
        else:
            self.matrix = [['*' for _ in range(dim)] for _ in range(dim)]

    def set_element(self, i, j, elem):
        """
        Setter pentru element
        :param i: rand
        :param j: coloana
        :param elem:
        :return:
        """
        self.matrix[i][j] = elem

    def get_element(self, i, j):
        """
        Getter pentru element
        :param i: rand
        :param j: coloana
        :return: elementul
        """
        return self.matrix[i][j]

    def set_line(self, line, elems):
        """
        Seteaza o linie
        :param line: index-ul liniei
        :param elems: elementele
        :return:
        """
        self.matrix[line] = [elem for elem in elems]

    def check_incomplete_line(self, row):
        """
        Verifica daca o linie incompleta ar putea deveni parte din solutie
        :param row: index-ul liniei
        :return: True sau False
        """
        line_set = {elem for elem in self.matrix[row] if elem != '*'}
        line = [elem for elem in self.matrix[row] if elem != '*']
        return len(line_set) == len(line)

    def check_incomplete_column(self, colnr):
        """
        Verifica daca o coloana incompleta ar putea deveni parte din solutie
        :param colnr: index-ul coloanei
        :return: True sau False
        """
        col = [row[colnr] for row in self.matrix if row[colnr] != '*']
        col_set = {row[colnr] for row in self.matrix if row[colnr] != '*'}
        return len(col) == len(col_set)

    def _find_left_upper_edge(self, i, j):
        """
        calculeaza coltul din stanga sus pentru i, j
        :param i:
        :param j:
        :return:
        """
        while i % self.area_dim != 0:
            i -= 1
        while j % self.area_dim != 0:
            j -= 1
        return i, j

    def check_incomplete_area(self, i, j):
        """
        Verfica o zona din Sudoku
        :param i:
        :param j:
        :return:
        """
        start_row, start_col = self._find_left_upper_edge(i, j)

        elems = [self.matrix[row][col] for row in range(start_row, start_row + self.area_dim)
                 for col in range(start_col, start_col + self.area_dim) if self.matrix[row][col] != '*']

        return len(elems) == len(set(elems))

    def del_element(self, i, j):
        """
        Sterge un element din matrice (reprezentare interna)
        :param i: rand
        :param j: coloana
        :return:
        """
        self.matrix[i][j] = '*'

    def is_free(self, i, j):
        """
        Verifica daca elementul de pe i,j este liber
        :param i: rand
        :param j: coloana
        :return:
        """
        return self.matrix[i][j] == '*'

    def get_dim(self):
        """
        :return: dimensiunea tabelei de joc
        """
        return self.dim

    def get_copy(self):
        """
        :return: O copie a jocului curent
        """
        return Sudoku(self.dim, deepcopy(self.matrix))

    def colect_free_pozs(self):
        """
        Colecteaza toate pozitiile libere
        :return: o lista de tuple, de genul (rand, coloana)
        """
        return [(i, j) for i, row in enumerate(self.matrix) for j, elem in enumerate(row) if elem == "*"]

    def is_solution(self):
        """
        Verifica daca e solutie
        :return:
        """
        return '*' not in str(self.matrix)

    def calculate_score(self):
        """
        Calculeaza scorul unui sudoku (higher is better)
        + 1 pentru fiecare linie sau coloana completa deja !
        :return:
        """
        score = 0
        score += sum(1 for line in self.matrix if len(line) == len(set(line)) == self.dim and '*' not in str(line))
        score += sum(1 for col in zip(*self.matrix) if len(col) == len(set(col)) == self.dim and '*' not in str(col))
        return score

    def __str__(self):
        """
        Folosit pentru afisare
        :return:
        """
        return "\n".join(" ".join(map(str, line)) for line in self.matrix)


def test_sudoku():
    s = Sudoku(4)
    s.set_element(0, 0, 1)
    assert s.get_element(0, 0) == 1

    s.del_element(0, 0)
    assert s.is_free(0, 0)

    s.set_line(0, [1, 2, 3, 4])
    assert s.get_element(0, 3) == 4
    assert s.check_incomplete_line(0)

    assert len(s.colect_free_pozs()) == 4 * 3

    s.set_line(1, [4, 3, 2, 1])
    s.set_line(2, [3, 4, 1, 2])
    s.set_line(3, [2, 1, 4, 3])

    assert s.is_solution()

    s2 = Sudoku(4)
    s2.set_line(0, [1, 2, 3, 4])
    s2.set_element(1, 0, 2)
    s2.set_element(2, 0, 3)
    # s2.set_element(3, 0, 4)

    assert s2.calculate_score() == 1

    s3 = Sudoku(9)
    assert s3._find_left_upper_edge(8, 7) == (6, 6)
    assert s3._find_left_upper_edge(4, 8) == (3, 6)
    assert s3._find_left_upper_edge(1, 5) == (0, 3)

    s3.set_element(8, 8, 1)
    assert s3.check_incomplete_area(8, 7)
    s3.set_element(8, 7, 1)
    assert not s3.check_incomplete_area(8, 8)

    s3.set_element(1, 2, 4)
    assert s3.check_incomplete_area(1, 1)
    s3.set_element(1, 3, 4)
    assert s3.check_incomplete_area(1, 1)
    s3.set_element(2, 0, 4)
    assert not s3.check_incomplete_area(1, 2)

    print_green("Test sudoku passed successfully")


if __name__ == "__main__":
    test_sudoku()