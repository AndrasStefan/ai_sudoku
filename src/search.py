from queue import Queue
from anytree import Node

from src.utils import print_green, timing


class SearchMethod:
    @staticmethod
    def _next_step_from_node(node, dim, all=set()):
        """
        Genereaza fii unui node, urmatoarele stari
        :param node: nodul de start
        :param dim: dimensiunea sudoku
        :param all: noduri generate deja, folosit pentru a nu genera de 2 ori aceeasi combinatie
        :return:
        """
        free = node.name.colect_free_pozs()

        generated_states = []
        if free:
            for i in range(1, dim + 1):

                for row, col in free:
                    child = node.name.get_copy()
                    child.set_element(row, col, str(i))

                    if str(child) not in all and child.check_incomplete_line(row) and child.check_incomplete_column(
                            col) and child.check_incomplete_area(row, col):
                        generated_states.append(Node(child, parent=node))

        return generated_states


class UninformedSearchMethod(SearchMethod):
    pass


class InformedSearchMethod(SearchMethod):
    pass


class BFS(UninformedSearchMethod):
    def __init__(self, root, dim=0):
        """
        Initializare pentru BFS
        :param root: sudoku initial, radacina arbore
        :param dim: dimensiune sudoku
        """
        self.root = root
        self.dim = dim

    def traverse(self):
        """
        Parcurge graful (deja construit) in latime
        :return: o lista, cu nodurile in ordine
        """
        q = Queue()
        q.put(self.root)
        rez = []

        while q.qsize():
            current = q.get()
            rez.append(current.name)

            for child in current.children:
                q.put(child)

        return rez

    @timing
    def solve(self):
        """
        Rezolva un sudoku
        :return: Sudoku rezolvat
        """
        current_level = Queue()
        all = set()

        current_level.put(self.root)
        all.add(str(self.root.name))

        rez = None
        while current_level.qsize() > 0:
            current_node = current_level.get()
            descends = self._next_step_from_node(current_node, self.dim, all)

            if descends:
                for elem in descends:
                    current_level.put(elem)
                    all.add(str(elem.name))
            elif current_node.name.is_solution():
                rez = current_node
                break

        return rez


class BestFS(InformedSearchMethod):
    pass


class GBFS(BestFS):
    def __init__(self, root, dim=0):
        """
        Initializare pentru GBFS
        :param root: sudoku initial, radacina arbore
        :param dim: dimensiune sudoku
        """
        self.root = root
        self.dim = dim

    @staticmethod
    def _choose_best(nodes):
        """
        Choosing the first one, sometimes works, LUCK !!
        :param nodes:
        :return:
        """
        return nodes[0]  # TODO: find a good way to choose

    @staticmethod
    def _choose_best_by_score(nodes):
        """
        Choosing the node with the best score.
        -- it doesn't work when the nodes are having the same score..
        :param nodes: all nodes
        :return: best one
        """
        max = 0
        rez = nodes[0]
        for elem in nodes:
            aux = elem.name.calculate_score()
            if aux > max:
                max = aux
                rez = elem
        return rez

    @timing
    def solve(self):
        """
        Rezolva un sudoku
        :return: sudoku rezolvat
        """
        current_node = self.root

        while True:
            descends = self._next_step_from_node(current_node, self.dim)
            if not descends:
                rez = current_node
                break
            current_node = self._choose_best_by_score(descends)

        return rez


def test_bfs_traverse():
    node1 = Node(1)
    node2 = Node(2, parent=node1)
    node4 = Node(4, parent=node2)
    node3 = Node(3, parent=node1)
    node5 = Node(5, parent=node3)
    node6 = Node(6, parent=node3)
    node7 = Node(7, parent=node3)

    assert BFS(node1).traverse() == list(range(1, 8))
    assert BFS(node3).traverse() == [3, 5, 6, 7]
    assert BFS(node2).traverse() == [2, 4]
    assert BFS(node7).traverse() == [7]

    print_green("Test traverse passed successfully")


if __name__ == "__main__":
    test_bfs_traverse()
