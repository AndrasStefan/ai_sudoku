import subprocess

from anytree import Node
from anytree.dotexport import DotExporter

from src.search import BFS, GBFS
from src.sudoku import Sudoku
from src.utils import print_green, timing


def get_sudoku_from_file(path):
    with open(path) as file:
        dim = int(file.readline())
        game = Sudoku(dim)

        for i, line in enumerate(file):
            game.set_line(i, line.split())

        return game

@timing
def handle(kind):
    file = input("Nume fisier: ")
    path = "../boards/" + file + ".txt"
    s = get_sudoku_from_file(path)
    root = Node(s)

    if kind == "BFS":
        solver = BFS(root, root.name.get_dim())
    else:
        solver = GBFS(root, root.name.get_dim())

    sol = solver.solve()

    DotExporter(sol).to_picture("../pictures/game.png")
    subprocess.Popen(['xdg-open', '../pictures/game.png'])
    return sol


while True:
    print("0. Quit")
    print("1. Run BFS")
    print("2. Run GBFS")

    cmd = input("Comanda este: ")

    if cmd == '1':
        handle("BFS")
    elif cmd == '2':
        sol = handle("GBFS")
        print_green("Este solutie") if sol.name.is_solution() else print_green("Nu este")
    elif cmd == '0':
        break

    else:
        print("Comanda invalida")
